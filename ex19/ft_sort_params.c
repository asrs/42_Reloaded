/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/16 15:48:11 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/06 18:05:56 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void			ft_putchar(char c);

void			ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

int				ft_strcmp(char *s1, char *s2)
{
	while (*s1 == *s2 && *s1)
	{
		s1++;
		s2++;
	}
	return ((int)(*s1 - *s2));
}

void			ft_display(char **av)
{
	int			i;

	i = 1;
	while (av[i])
	{
		ft_putstr(av[i++]);
		ft_putchar('\n');
	}
}

int				main(int ac, char **av)
{
	int			i;
	int			j;
	char		*tmp;

	i = 1;
	while (i < ac)
	{
		j = 0;
		while (j < (ac - 1))
		{
			if (ft_strcmp(av[j], av[j + 1]) > 0)
			{
				tmp = av[j];
				av[j] = av[j + 1];
				av[j + 1] = tmp;
			}
			j++;
		}
		i++;
	}
	ft_display(av);
	return (0);
}
