/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/16 00:13:26 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/06 14:22:29 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_recursive_factorial(int nb)
{
	int			result;

	if (nb == 0 || nb == 1)
		return (1);
	else if (nb >= 2 && nb < 13)
		result = (nb * ft_recursive_factorial(nb - 1));
	else
		return (0);
	return (result);
}
