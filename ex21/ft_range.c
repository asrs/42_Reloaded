/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/16 17:34:06 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/06 11:53:11 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int				*ft_range(int min, int max)
{
	int			*tab;
	int			i;
	int			range;

	i = 0;
	if (min >= max)
		return (NULL);
	range = max - min;
	if (!(tab = (int *)malloc(sizeof(int) * (max - min))))
		return (NULL);
	while (min < max)
		tab[i++] = min++;
	return (tab);
}
