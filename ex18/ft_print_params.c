/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/16 15:31:40 by clrichar          #+#    #+#             */
/*   Updated: 2017/11/06 15:03:37 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void		ft_putchar(char c);

void		ft_putstr(char *str)
{
	while (*str)
		ft_putchar(*str++);
}

int			main(int ac, char **av)
{
	int		i;

	i = 1;
	ac -= i;
	while (ac--)
	{
		ft_putstr(av[i++]);
		ft_putchar('\n');
	}
	return (0);
}
